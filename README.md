# GuiTor, the Guitar tuTor: a Guitar Learning Recommender System

Recommend learnable songs.

## Guitar Social Club

This work comes from a collaboration with the [Guitar Social Club](https://guitarsocialclub.com), founded by
Yohann Abbou and Gilles Guillemain.

## Configuration

This project uses Python 3.10.
Set up your environment with:

```bash
pip install -r requirements.txt
```

_You might want to setup a virtual env before doing that._

## Usage

Run commands from the root folder.

```bash
python main.py -v
```

This will execute the code and save the results in the `data` folder.

You can make a custom profile by mimicking the default one in `data/profiles` and use the `-C` argument.
You can also use your own data
by replacing the source file in `data` (`-c` or `--corpus` argument).

## License
- This code is distributed under the LGPLv3+ License. Refer to the LICENSE file for more details.


## Attribution

If you use this repository, consider citing our paper presented at the [JIM 2024](https://jim2024.sciencesconf.org):
```Bibtex
@inproceedings{dhooge_suggestion-guitare_2024,
  title = {Suggestions pédagogiques personnalisées pour la guitare},
  booktitle = {Journées d'Informatique Musicale (JIM 2024)},
  author = {D'Hooge, Alexandre and Giraud, Mathieu and Abbou, Yohann and Guillemain, Gilles},
  year = {2024},
  address = {Marseille, France},
}
```

You can find this paper (in French) in the official proceedings or [here](https://hal.science/hal-04564669)

## Acknowledgements

This work is partly funded under the ANR Tabasco Project: [ANR-22-CE38-0001](https://anr.fr/Projet-ANR-22-CE38-0001).
