'''
Recommend learnable songs.
Copyright (C) 2023 by Algomus Team at CRIStAL (Université Lille, UMR CNRS 9189),
in collaboration with Guitar Social Club (GSC) and Weaverize.
Contributors:
    Alexandre d'Hooge <alexandre@algomus.fr>
    Mathieu Giraud <mathieu@algomus.fr>

This is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 
'''

import argparse
import csv
from datetime import datetime
import json
import sys

import pandas as pd
from rich.console import Console

from guitor.corpus import read_corpus, add_song_info
from guitor.model import CHALLENGES
from guitor.user import User, ellipsis_str
from guitor.util import tag_stats

PROFILE_CSV = 'data/profiles/sarah.csv'
NOW = datetime.now().strftime('%Y-%m-%d')
TRACE = 'data/out-%s.html' % NOW
CSV = 'data/out-%s.csv' % NOW


parser = argparse.ArgumentParser(
                    description='Recommend learnable songs. Code by Algomus Team, distributed under LGPLv3+.')

parser.add_argument('-C', '--profile-csv',  action="store_true",
                    help=f"Source csv file for profile. Default is {PROFILE_CSV}.")
parser.add_argument('-c', '--corpus', default="data/corpus_release.csv", metavar='CORPUS-FILE',
                    help="corpus .csv, with song ratings (%(default)s)")
parser.add_argument('-n', '--nb', type=int, default=12,
                    help="number of proposals (%(default)s)")
parser.add_argument('-S', '--scores', action="store_true",
                    help="show score computation")

parser.add_argument('-s', '--show', type=str, action='append', metavar='ID',
                    help="show song details")
parser.add_argument('-o', '--out', default="data/out.json", metavar='OUT-FILE',
                    help="out .json, with song proposals (%(default)s)")
parser.add_argument('--csv', type=str, default=CSV, metavar="OUT-CSV-FILE",
                    help="out .csv, with song proposals (%(default)s)")
parser.add_argument('-j', '--verbose-json', action="store_true",
                    help="verbose json output")
parser.add_argument('-v', '--verbose', action="store_true",
                    help="verbose output")


def profiles_from_csv(f_csv):
    profiles = []
    with open(f_csv, 'r', newline='', encoding='utf-8-sig') as f:
        reader = csv.DictReader(f, delimiter=";")
        profile = {'meta': None, 'pieces': []}
        for row in reader:
            if profile['meta'] is not None and row['Nom'] != profile['meta']['name']:
                # switching to a new profile
                profiles.append(profile)
                profile = {'meta': None, 'pieces': []}
            else:
                if profile['meta'] is None:
                    profile['meta'] = {'name': row['Nom']}
                song_id = row["#Morceau"]
                tag = row['Type']
                # data correction
                if tag == 'Init':
                    tag = 'initial'
                if song_id != '':
                    profile['pieces'].append({'id': song_id, 'tag': tag})
    profiles.append(profile)
    return profiles

if __name__ == '__main__':

    console = Console(record=True)
    print = console.print
    args = parser.parse_args()
    
    ## Read corpus
    corpus = read_corpus(args.corpus)
    if args.verbose:
        print(ellipsis_str(list(corpus.keys()), 30))
    print()

    profiles = []
    out_csv = None

    print('[purple]<== profiles:[/purple]', PROFILE_CSV)
    profiles += profiles_from_csv(PROFILE_CSV)


    print('==>', ', '.join([d['meta']['name'] for d in profiles]))
    print()

    for profile in profiles:
        ## Profile from .json
        user0 = User()
        print('[purple]<== profile:[/purple]', profile['meta']['name'])
        user0.read_profile_json(profile, corpus=corpus)
        print('<==', user0.str_stats())
        user0.compute_levels()

        if args.show:
            # Only show a song, take the first user profile
            songs = []
            for i in args.show:
               songs += [corpus[i]]
               songs += corpus[i].parts
            print(user0.str_output(songs, nb=len(songs),
                                   detail_scores=args.scores,
                                   by_challenge = True,
                                   all_challenges = True))
            continue

        learnable = user0.learnable_songs(corpus,
                                          nb_by_challenge=(args.nb//len(CHALLENGES.keys())+args.nb%len(CHALLENGES.keys())))
        if args.verbose:
            print(user0.str_profile())
            print(user0.str_output(learnable, nb=args.nb, detail_scores=args.scores))

        # Output to .json file
        if args.out:
            out_json = user0.output(learnable, nb=args.nb)
            print('[purple]==> proposals:[/purple]', args.out)
            print('==>', tag_stats(out_json))

            if not 'log' in out_json.keys():
                out_json['log'] = {}
            out_json['log'].update({
                'date': str(datetime.now()),
                'cmd': ' '.join(sys.argv),
            })

            json.dump(out_json, open(args.out, 'w'), indent=2)
            if args.verbose_json:
                print(json.dumps(out_json, indent=2))
        print()

        if args.csv:
            tmp_df = user0.csv_output(learnable, nb=args.nb)
            tmp_df = add_song_info(corpus, tmp_df)
            out_csv = tmp_df if out_csv is None else pd.concat([out_csv, tmp_df], ignore_index=True)

    if args.csv and out_csv is not None:
        print('[purple]==> proposals (.csv):[/purple]', args.csv)
        out_csv.to_csv(args.csv)

    # Save trace
    print('[purple]==> trace:[/purple]', TRACE)
    console.save_html(TRACE)
