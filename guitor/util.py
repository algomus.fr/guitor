from typing import Dict, List
from guitor.model import LEVEL_AVERAGE_BEST

def tag_stats(j: Dict) -> str:
    """tag_stats.
    Get number of suggestions per tags from a dict
    which corresponds to the standard json output of the model.

    Args:
        j (Dict): json output produced by the model.
                  It should contain a list of songs at the 'pieces' key
                  where each one has a specific 'tag'.

    Returns:
        str: 'tag' followed by its number of occurences.
    """
    pieces = j['pieces']
    tags = [p['tag'] for p in pieces]
    unique = sorted(list(set(tags)))
    return ' '.join([f'{t}:{tags.count(t)}' for t in unique])


def get_song_from_id(corpus: Dict[str, 'Song'], song_id: str,
                     include_full: bool = False) -> List['Song']:
    """get_song_from_id.
    Return list of Song objects matching provided id.
    If id corresponds to a full song, all parts are returned.
    Otherwise, list only has one element.

    Args:
        corpus (Dict[str, 'Song']): corpus with songs index by their id;
        song_id (str): song_id, in the format XXX.Y or XXX;
        include_full (bool): include_full. Switch to include the FullSong
                             in the output list. Only works if id is XXX.

    Returns:
        List['Song']: list of Song objects matching song_id.
    """
    out = []
    if '.' in song_id:
        out.append(corpus[song_id])
    elif include_full:
        try:
            out.append(corpus[song_id])
        except KeyError:
            # might be that the full song is not there because it only has one part
            # will raise a new KeyError otherwise
            out.append(corpus[song_id+'.'+str(1)])
    if not '.' in song_id:
        # all parts of the song are known
        part = 1
        while True:
            try:
                out.append(corpus[song_id+'.'+str(part)])
                part += 1
            except KeyError:
                # No more parts available
                break
    return out

def ellipsis_list(l, start=10, end=0) -> List:
    """ellipsis_list.
    Shorten a list by removing some of the middle values.

    Args:
        l: list to shorten;
        start: number of elements at the start to keep. Defaults to 10;
        end: number of elements to show from the end. Defaults to 0.

    Returns:
        List:
    """
    if len(l) <= start + end:
        return l
    ll = []
    if start:
        ll += l[:start]
    ll += ['...']
    if end:
        ll += l[-end:]
    return ll

def ellipsis_str(l, start=10, end=0, lines=False, f=str) -> str:
    """ellipsis_str.
    Shorten a list by removing some of the middle values and
    return a string with the shortened content.

    Args:
        l: list to shorten;
        start: number of elements at the start to keep. Defaults to 10;
        end: number of elements to show from the end. Defaults to 0.
        lines: enable multi-line output. Default is False.
        f: type to map the list elements to. Default is str.

    Returns:
        str:
    """
    s = '(%d): ' % len(l)
    sep = '\n  ' if lines else ', '
    if lines:
        s += sep
    s += sep.join(map(f, ellipsis_list(l, start, end)))
    if lines:
        s += '\n'
    return s

def average_best(l, n=LEVEL_AVERAGE_BEST) -> float:
    """average_best.
    Returns the mean of the top-n values in l.

    Args:
        l: list to average;
        n: number of top values to consider. Default is LEVEL_AVERAGE_BEST from src.model.

    Returns:
        float:
    """
    if not l:
        return 0
    ll = sorted(l)[-n:]
    return sum(ll) / len(ll)

