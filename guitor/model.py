from math import exp
from typing import Callable

LEVEL_AVERAGE_BEST = 15

WISH_BONUS = 1.00
FULLSONG_BONUS = 1.00
MAX_BONUS = 2.00

LEVELS = ['tempo',  'rhythm', 'chord', 'difficulty', 'fig_rhythm', 'song' ,'bonus', 'style']

# constants to distinguish songs
TAG_INIT = 'initial'
TAG_WORK = 'work'
TAG_WISH = 'wish'
TAG_PROPOSAL = 'proposal'

def DIFF_EXP_(a: float, b:float,
            beta: float = 1.0, gamma: float = 0.0) -> float :
    """
    An exponential difficulty function to evaluate how hard it is to learn a
    new song. a,b should be some_rating, user_level.

    Args:
        a, b (float): difficulties;
        beta (float): Scaling coefficient. Default is 1;
        gamma (float): bias coefficient. Default is 0.

    Returns:
        float: y = \\exp{\\beta*(\\delta - \\gamma)} - 1) with \\delta = b - a
    """
    delta = b if a is None else b-a
    y = exp(beta*(delta - gamma)) - 1
    return y

def DIFF_LIN_(a: float, b:float, gamma: float = 0.0) -> float :
    """DIFF_LIN_.
    Linear difficulty function.

    Args:
        a (float): a, a rating. float or None if not necessary.
        b (float): b, user level.
        gamma (float): bias coefficient. Default is 0

    Returns:
        float: y = \\delta - \\gamma with \\delta = b - a
    """
    delta = b if a is None else b-a
    return (delta-gamma)

def DIFF_EXP(beta: float = 1.0, gamma: float = 0.0) -> Callable :
    """DIFF_EXP.
    Return lambda function of exponential difficulty with beta
    and gamma parameters set.

    Args:
        beta (float): scaling coefficient. Default is 1.
        gamma (float): bias coefficient. Default is 0.

    Returns:
        Callable: lambda function.
    """
    return lambda a,b: DIFF_EXP_(a, b, beta, gamma)

def DIFF_LIN(gamma: float = 0.0) -> Callable :
    """DIFF_LIN.
    Return lambda function of linear difficulty with gamma parameter set.

    Args:
        gamma (float): bias coefficient. Default is 0.

    Returns:
        Callable: lambda function.
    """
    return lambda a,b: DIFF_LIN_(a, b, gamma)

def STYLE(styles: dict[int, float], s: int) -> float:
    """STYLE.
    Style penalty function. The less the style s is played, the closer
    the output will be to 1.

    Args:
        styles (dict[int, float]): ratio of songs played in each style.
        s (int): style to compute the penalty for.

    Returns:
        float: penalty between 0 and 1. Favorite styles should give
               a value close to 0.
    """
    return 1 - styles[s]

def IGNORE(_a, _b) -> float:
    """IGNORE.
    Dummy difficulty function that always return 0.
    """
    return 0

# The challenge level that is considered standard.
# Below is easy and above is hard.
CHALLENGE_DEFAULT = 2

# Manually set coefficients and difficulty functions for
# each challenge level and difficulty dimension.
CHALLENGES = {
    1: {
        #                alpha,           beta, gamma
        'tempo':      (  0.5,   DIFF_EXP( 2.0,  -1.0)),
        'rhythm':     (  0.5,   DIFF_EXP( 1.5,  -1.0)),
        'chord':      (  0.5,   DIFF_EXP( 1.0,  -1.0)),
        'difficulty': (  0.8,   DIFF_EXP( 0.3,  -1.0)),

        'fig_rhythm': (  0.5,   DIFF_EXP( 1.5,  -1.0)),
        'song':       (  0.4,   DIFF_EXP( 0.3,  0.0)),

        'bonus':      (  4.0,   DIFF_LIN(MAX_BONUS)),
        'style':      (  1.5,   STYLE),
    },
    2: {
         #                 alpha,           beta, gamma
        'tempo':  (        0.5,   DIFF_EXP( 2.0,  0.0)),
        'rhythm': (        0.5,   DIFF_EXP( 1.5,  0.0)),
        'chord':  (        0.5,   DIFF_EXP( 1.0,  0.0)),
        'difficulty':   (  0.8,   DIFF_EXP( 0.3,  0.5)),

        'fig_rhythm': (    0.5,   DIFF_EXP( 1.5,  0.0)),
        'song':   (        0.8,   DIFF_EXP( 0.3,  3.0)),

        'bonus':  (        0.5,   DIFF_LIN(MAX_BONUS)),
        'style':  (        1.5,   STYLE),
    },
    3: {
         #                 alpha,         beta, gamma
        'tempo':  (        0.5,   DIFF_EXP( 2.0,  1.0)),
        'rhythm': (        0.5,   DIFF_EXP( 1.5,  1.0)),
        'chord':  (        0.5,   DIFF_EXP( 1.0,  1.0)),
        'difficulty':   (  0.8,   DIFF_EXP( 0.3,  1.0)),

        'fig_rhythm': (    0.5,   DIFF_EXP( 1.5,  1.0)),
        'song':   (        0.8,   DIFF_EXP( 0.3,  6.0)),

        'bonus':  (        1.0,   DIFF_LIN(MAX_BONUS)),
        'style':  (        1.5,   STYLE),
    },
    4: {
         #                alpha,         beta, gamma
        'tempo':  (      0.5,   DIFF_EXP( 2.0,  1.0)),
        'rhythm': (      0.5,   DIFF_EXP( 1.5,  2.0)),
        'chord':  (      0.5,   DIFF_EXP( 1.0,  2.0)),
        'difficulty': (  0.8,   DIFF_EXP( 0.3,  1.0)),

        'fig_rhythm': (  0.5,   DIFF_EXP( 1.5,  2.0)),
        'song':   (      0.8,   DIFF_EXP( 0.3,  9.0)),

        'bonus':  (      4.0,   DIFF_LIN(MAX_BONUS)),
        'style':  (      1.5,   STYLE),
    }
}
