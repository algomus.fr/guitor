from rich import text

def derich(s):
    """
    Remove color annotations from string.
    """
    return text.Text.from_markup(s).plain
