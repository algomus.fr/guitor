'''
Recommend learnable songs.
Copyright (C) 2023 by Algomus Team at CRIStAL (Université Lille, UMR CNRS 9189),
in collaboration with Guitar Social Club (GSC) and Weaverize.
Contributors:
    Alexandre d'Hooge <alexandre@algomus.fr>
    Mathieu Giraud <mathieu@algomus.fr>

This is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 
'''

import math
from typing import List, Dict
from enum import Enum
from abc import ABC, abstractmethod
import pandas as pd

from guitor.model import LEVELS, CHALLENGE_DEFAULT, FULLSONG_BONUS

def str_levels(items, long=False, format='%d', ref=None, levels=LEVELS):
    it = list(items)
    # Sort by the order in LEVELS
    it.sort(key = lambda kv: levels.index(kv[0]))

    def format_level(k, v):
        if v is None:
            return '·'
        if k == 'style' or k == 's':
            try:
                return ('S' + format) % v
            except TypeError:
                return 'S{%s}' % (','.join(['%s:%.2f' % (kk, vv) for kk, vv in v.items()]))
        if v is None or math.isnan(v):
            return '·'
        if not ref:
            color = 'blue'
        else:
            diff = v - ref[k] if ref[k] else v
            if k[0] == 'e': # exercices 
                diff -= 2
            if k[0] == 's': # song level
                diff /= 2
            if diff < 0:
                color = 'white'
            elif diff < 0.6:
                color = 'blue'
            elif diff < 2:
                color = 'green'
            else:
                color = 'red'

            if k[0] == 'b': # bonus
                if diff < 0.5:
                    color = 'white'
                elif diff < 1.5:
                    color = 'blue'
                else:
                    color = 'green'

        fmt = '[%s]' % color + format + '[/]'
        return ('%s' + fmt) %  (k if long else k[0], v)

    s = '/'.join([format_level(k, v) for (k, v) in it])
    return s


class Style(Enum):
    """
    Enumerate-like structure
    to translate the Style information.
    """
    UNDEFINED = 0
    POP = 1
    FUNKSOULRB = 2
    WORLDMUSIC = 3
    RADIO = 4
    ROCK = 5
    BLUES = 6

# Dictionary to map a string to a Style object
STR2STYLE = {
        "Pop": Style.POP,
        "Funk/Soul/R&B": Style.FUNKSOULRB,
        "Musiques du monde": Style.WORLDMUSIC,
        "Radio": Style.RADIO,
        "Rock": Style.ROCK,
        "Blues": Style.BLUES
        }


def filter_complete(l):
    '''
    Filter a sorted set of songs, keeping only the first item with the same base song
    '''
    out = []
    ids = []
    for song in l:
        i = song.full_song_id()
        if not i in ids:
            out += [song]
            ids += [i]
    return out

class Song(ABC):
    """
    General Song Abstract Base Class (ABC).
    """
    main_rhythm: float   # difficulty coeff of the main rhythmic figure
    ref_levels = LEVELS

    def get_level(self, user) -> Dict:  # type: ignore
        """
        Dictionary of levels in the different categories
        """
        return {

            # manual
            'tempo': self.tempo_rating,
            'rhythm': self.rhythm_rating,
            'chord': self.chord_rating,
            'difficulty': self.difficulty,

            # semi-automatic
            'fig_rhythm': self.main_rhythm,
            'song': self.song_rating,

            # other
            'bonus': self.bonus 
                     + (user.bonus[self.id] if user and self.id in user.bonus else 0),
            'style': self.style.value,
        }

    def str_levels(self, user=None) -> str:
        """str_levels.
        Produce a more readable output of the levels associated
        with the song.

        Args:
            self:
            user: Optional User to compute some of the levels. Default is None.

        Returns:
            str: one line output of the levels in each category.
        """
        return str_levels(self.get_level(user).items(), levels=self.ref_levels)

    @property
    @abstractmethod
    def tempo_rating(self) -> int:
        """
        Attribute (or property) representing the rating of
        the Song regarding tempo.
        """
        pass

    @property
    @abstractmethod
    def chord_rating(self) -> int:
        """
        Attribute (or property) representing the rating of
        the Song regarding chords used.
        """
        pass

    @property
    @abstractmethod
    def rhythm_rating(self) -> int:
        """
        Attribute (or property) representing the rating of
        the Song regarding rhythm.
        """
        pass

    @property
    @abstractmethod
    def song_rating(self) -> float:
        """
        Attribute (or property) representing the rating of
        the Song in a general sense.
        """
        pass

    @property
    @abstractmethod
    def difficulty(self) -> int:
        """
        Difficulty of a song, as rated by the expert teacher.
        """
        pass

    @property
    def style(self) -> Style:
        """
        Property to access the style of a song.
        Defaults to UNDEFINED.
        """
        return Style.UNDEFINED


class PartSong(Song):
    """
    Part of a Song.
    Unique labels are in the shape XXX.Y
    """
    def full_song_id(self) -> str:
        """full_song_id.
        XXX part of the full id (XXX.Y)

        Args:
            self:

        Returns:
            str: id of the FullSong it belongs to.
        """
        return self.id[:self.id.index('.')]

    @staticmethod
    def parse_style(val: str) -> Style:
        """
        Convert input string to Style enum value.
        """
        return STR2STYLE.get(val, Style.UNDEFINED)

    def __init__(self) -> None:
        self.song_id: str = ''
        self.part_id: str = ''
        self.title: str = ''
        self.artist: str = ''
        self.section: str = ''
        self._difficulty: int
        self._style: Style
        self.bonus: float = 0
        self.challenge = CHALLENGE_DEFAULT
        self.exclude : bool = False
        self._song_rating: float
        self._tempo_rating: int
        self._rhythm_rating: int
        self._chord_rating: int
        self.main_rhythm: float


    @property
    def song_rating(self):
        return self._song_rating

    @property
    def tempo_rating(self):
        return self._tempo_rating

    @property
    def rhythm_rating(self):
        return self._rhythm_rating

    @property
    def difficulty(self):
        return self._difficulty

    @property
    def chord_rating(self):
        return self._chord_rating

    @property
    def id(self) -> str:
        return self.song_id + '.' + self.part_id

    @property
    def style(self) -> Style:
        return self._style
    
    def parse_id(self, i) -> str:
        """parse_id.
        get PartSong id from dataframe value.
        Check value type before doing conversion.

        Args:
            self:
            i: id value, could be string or float.

        Returns:
            str:
        """
        id = ''
        if isinstance(i, str):
            id = i
        elif isinstance(i, float):
            id = str(i)[:-2]   # drop trailing .0
        return id

    def from_df(self, df) -> None:
        """
        Set all attributes from a dataframe row and another dataframe
        with all exercises.
        """
        self.song_id = str(df['PartSongID'])
        self.part_id = str(df['PartPartID'])
        self.title = df['PartSongLabel']
        self.artist = df['PartSongArtist']
        self.main_rhythm = df['PartCoefFigRythm']
        self.section = df['PartLabel']
        self._difficulty = df['PartNoteDifficulte']
        self._song_rating = df['PartNotePartie'] 
        self._tempo_rating = df['PartNoteTempo'] if not pd.isna(df['PartNoteTempo']) else 0
        self._rhythm_rating = df['PartNoteRythme'] if not pd.isna(df['PartNoteRythme']) else 0
        self._chord_rating = df['NoteNatureAccord'] if not pd.isna(df['NoteNatureAccord']) else 0
        self._style = PartSong.parse_style(df['PartStyle'])
        try:
            self.exclude = (df['PartExcluIA'] == 'X')
        except KeyError:
            self.exclude = False

    def repr(self, user=None) -> str:
        s = ''
        s += str_levels(self.get_level(user).items(), ref=user.level if user else None, levels=self.ref_levels)
        if user and self.id in user.bonus:
            s+= " [green]<wish>[/]"
        s += f" [purple]{self.id:>5s}[/] [green]{self.title}[/]"
        if str(self.artist) != 'nan':
            s += f" by {self.artist}"
        if str(self.section) != 'nan':
            s += f" - [yellow]{self.section}[/]"
        if self.exclude:
            s += f" <X>"
        return s
    
    def repr_full(self, user=None) -> str:
        return self.repr(user)

    def __repr__(self) -> str:
        return self.repr()

    def __eq__(self, __o: object) -> bool:
        if not isinstance(__o, PartSong):
            return False
        return (__o.artist == self.artist and
                __o.title == self.title and
                __o.section == self.section)


class FullSong(Song):
    """
    Class representing a Complete Song from the dataset.
    It is composed of several PartSong but can be considered
    as a single Song object.
    """
    def __init__(self) -> None:
        self.id: str
        self.title: str = ''
        self.challenge = CHALLENGE_DEFAULT
        self.parts: List[PartSong] = []
        self.bonus = FULLSONG_BONUS
        self.artist: str
        self.exclude: bool = False

    def full_song_id(self) -> str:
        return self.id

    def __len__(self):
        return len(self.parts)

    def __iter__(self):
        return self.parts.__iter__()

    def __getitem__(self, i):
        return self.parts[i]

    @property
    def tempo_rating(self) -> int:
        return max([part.tempo_rating for part in self])

    @property
    def chord_rating(self) -> int:
        return max([part.chord_rating for part in self])

    @property
    def rhythm_rating(self) -> int:
        return max([part.rhythm_rating for part in self])

    @property
    def main_rhythm(self) -> float:
        return max([part.main_rhythm for part in self])
    
    @property
    def difficulty(self) -> int:
        return max([part.difficulty for part in self])

    @property
    def song_rating(self) -> float:
        return max([part.song_rating for part in self])

    @property
    def style(self) -> Style:
        """
        We derive the style of a FullSong from the style
        of its Parts (the first in that case).
        """
        return self.parts[0].style

    def repr(self, user=None) -> str:
        """
        Temporary implementation.
        """
        s = ''
        s += str_levels(self.get_level(user).items(), ref=user.level if user else None, levels=self.ref_levels)
        s += f" [red]{self.id:>5s}[/] [red]{self.title}[/] [{len(self)}]"
        if str(self[0].artist) != 'nan':
            s += f" by {self[0].artist}"
        return s
    
    def __repr__(self):
        return self.repr()
    
    def repr_full(self, user=None) -> str:
        s = self.repr(user) + '\n'
        for part in self:
            s += part.repr(user) + '\n'
        return s
        
