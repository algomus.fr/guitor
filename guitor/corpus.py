from collections import defaultdict
from typing import Dict

import pandas as pd
from rich import print

from guitor.song import FullSong, PartSong
from guitor.tools import derich


def add_song_info(corpus: dict, df: pd.DataFrame) -> pd.DataFrame:
    """
    Takes dataframe for csv tests from user.csv_output and
    fill song info columns based on the provided Corpus.
    """
    for i, row in df.iterrows():
        song = corpus[row['id']]
        row['Artist'] = song.artist if isinstance(song, PartSong) else song.parts[0].artist
        row['Song'] = song.title if isinstance (song, PartSong) else song.parts[0].title
        row['Part'] = song.section if isinstance(song, PartSong) else "FullSong"
        row['Levels'] = derich(song.str_levels())
        df.iloc[i] = row
    return df

def read_corpus(f1: str, decimal: str = '.', sep: str = ',') -> Dict:
    """read_corpus.
    Prepare the corpus dict from the csv source file.

    Args:
        f1: ref_part.csv file
        decimal: decimal char for parsing csv file. Default is '.'
        sep: sep char for parsing csv file. Default is ',' (like a proper Csv)
    """
    df = pd.read_csv(f1, decimal=decimal, sep=sep)

    corpus = defaultdict(FullSong)

    # Build parts
    songs = []
    for _, row in df.iterrows():
        song = PartSong()
        song.from_df(row)
        songs.append(song)
        corpus[song.id] = song

    # add full songs
    corpus = add_fullsongs(corpus)
    full_ids = set([song.full_song_id() for song in songs])
    nb_songs = len(full_ids)
    nb_parts = len(songs)

    print(f'<== {nb_songs} songs, {nb_parts} parts')
    return dict(corpus)


def add_fullsongs(corpus: dict) -> dict:
    corpus = defaultdict(FullSong, corpus)
    songs = list(corpus.values())
    for song in songs:
        full_id = song.full_song_id()
        full = corpus[full_id]
        full.id = full_id
        full.title = song.title
        full.parts.append(song)
        full.artist = song.artist
    return dict(corpus)


def read_oracle(f):
    """read_oracle.
    Load oracle file and store as a dict.

    Args:
        f: csv file with oracle information.
    """
    oracle = {}
    df = pd.read_csv(f, sep=",", dtype=str)
    for _, row in df.iterrows():
        cha = str(row['Challenge'])
        oracle[row['User'],str(row['id'])] = cha
        if cha not in '012345' and not '?' in cha:
            print(f"! {row['User']} {row['id']} [red] {row['Challenge']} [/]")
    return oracle

def write_oracle(f, oracle, corpus):
    """write_oracle.
    Store oracle info as a csv file.

    Args:
        f: path for csv file.
        oracle: dict with oracle on suggestions for selected users.
        corpus: corpus dict to get song related information.
    """
    l = []
    for ((user, i), challenge) in oracle.items():
        try:
            row = {'User': user, 'Challenge': challenge, 'id': i}
            row['Title'] = corpus[i].title
            if '.' in i:
                row['Part'] = corpus[i].section
            l.append(row)
        except KeyError:
            print(f"Song {i} no longer in corpus, skipping.")
    df = pd.DataFrame(l)
    df.to_csv(f, index=False)
