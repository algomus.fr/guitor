'''
Recommend learnable songs.
Copyright (C) 2023 by Algomus Team at CRIStAL (Université Lille, UMR CNRS 9189),
in collaboration with Guitar Social Club (GSC) and Weaverize.
Contributors:
    Alexandre d'Hooge <alexandre@algomus.fr>
    Mathieu Giraud <mathieu@algomus.fr>

This is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 
'''

from collections import defaultdict
from copy import deepcopy
import json
from typing import Dict, List

import pandas as pd
from rich import print

from guitor.model import CHALLENGES, TAG_INIT, TAG_PROPOSAL, TAG_WISH, TAG_WORK, WISH_BONUS
from guitor.song import Song
import guitor.song
from guitor.tools import derich
from guitor.util import average_best, ellipsis_str, get_song_from_id, tag_stats

class User():
    """
    Class for any user.
    """
    source_challenges = CHALLENGES
    def __init__(self, name: str='User') -> None:
        self.name = name
        self.profile = {}
        self.level: dict
        self.temp_level: int
        self.rhythm_level: int
        self.chord_level: int
        self.songs_known: List[Song] = []
        self.songs: Dict[str, List[str]] = {}         # key is song id, value is list of tags 
        self.bonus = {}
        self.challenges = User.source_challenges[2]

    def add_song(self, song: Song, tag: str) -> None:
        """add_song.
        Store song with the provided tag.

        Args:
            self:
            song (Song): song to add.
            tag (str): tag for the new song. Must be one of
                       ['work', 'wish', 'proposal', 'init']

        Returns:
            None:
        """
        if tag == TAG_WISH:
            # add bonus to wish song
            self.bonus[song.id] = WISH_BONUS
            return

        if tag not in [TAG_INIT, TAG_WORK, TAG_PROPOSAL]:
            #Unknown tag
            print(f'! Ignore {song.id} with tag "{tag}"')
            return

        self.songs_known.append(song)
        return

    def learn_songs(self, songs: List[Song], tag: str = TAG_WORK) -> None:
        """learn_songs.
        Call `add_song` on a list of Song objects with the same tag.

        Args:
            self:
            songs (List[Song]): songs to add;
            tag (str): tag for the new song. Must be one of:
                 ['work', 'wish', 'proposal', 'init']

        Returns:
            None:
        """
        for song in songs:
            self.add_song(song, tag)

    def read_profile_json(self, json_data: str | dict, corpus: Dict[str, Song] | None = None) -> None:
        """
        Read profile and learn songs from a json formatted string.
        args:
            json_data (str | dict): json-formatted string. Must contain a first 'pieces' key
                which value is a list of {'id': id, 'tag': tag}. Can also be directly
                a Python dict with the correct structure.
            corpus ({id:Song}): optional. Dictionary of Song objects that are 
                referenced by their id. If provided, the method will also populate
                the songs_known attribute.
        """
        if isinstance(json_data, str):
            profile = json.loads(json_data)
        else:
            profile = json_data
        self.profile = profile
        try:
            self.name = profile['meta']['name']
        except KeyError:
            pass

        for piece in profile['pieces']:
            piece['id'] = piece['id'].lstrip("0")
            self.songs[piece['id']] = piece['tag']
            if corpus is not None:
                songs = get_song_from_id(corpus=corpus, song_id=piece['id'],
                                         # also include the full piece
                                         include_full = (piece['tag'] in [TAG_INIT, TAG_WISH]))
                self.learn_songs(songs, piece['tag'])

    def learnable_songs(self, corpus, nb_by_challenge=3,
            challenges: dict=CHALLENGES):
        """learnable_songs.
        Get the list of best song recommendations for User, one list per
        challenge level (defined by src.model.CHALLENGES).

        Args:
            self:
            corpus: dict of songs. Key is song id and value is the corresponding
                    Song object;
            nb_by_challenge: number of suggestion to provide for each challenge.
                             Default is 3.
            challenges: reference dict with coefficients set for several challenges.
        """

        learnable = defaultdict(list)
        songs = list(corpus.values())

        for song in songs:
            if song in self.songs_known:
                continue
            if song.exclude:
                continue
            # Fit to the closest challenge
            fits = []
            for cha in challenges.keys():
                self.challenges = challenges[cha]
                fit = abs(self.song_difficulty(song))
                fits.append((fit, cha))

            fits.sort()
            song.fit, song.challenge = fits[0]
            learnable[song.challenge].append(song)

        out = []
        for cha in sorted(challenges.keys()):
            learnable[cha].sort(key = lambda song: song.fit)
            learnable[cha] = guitor.song.filter_complete(learnable[cha])
            out += learnable[cha][:nb_by_challenge]
        return out

    def compute_levels(self):
        """compute_levels.
        Get the level of the User in each considered category.
        Result is stored in self.level.
        """
        self.level = {
            'tempo': average_best([song.tempo_rating for song in self.songs_known]),
            'chord': average_best([song.chord_rating for song in self.songs_known]),
            'fig_rhythm': average_best([song.main_rhythm for song in self.songs_known]),
            'rhythm': average_best([song.rhythm_rating for song in self.songs_known]),
            'song': average_best([song.song_rating for song in self.songs_known]),
            'difficulty': average_best([song.difficulty for song in self.songs_known]),
            'style': self._style_pref(),
            'bonus': None,
        }

    def _style_pref(self) -> dict:
        """
        Return a dict that represents the styles of the user.
        TODO: Try something with a list and a soft argmax
        """
        out = defaultdict(float)
        if not self.songs_known:
            return out       
        ll = 1/len(self.songs_known)
        for song in self.songs_known:
            out[song.style.value] += ll
        return out

    def song_difficulty(self, song, return_dict: bool = False, detail_scores= False):
        """song_difficulty.
        Return the total difficulty of the user to play the provided song.
        Args:
            self: user with its own level in each category
            song: song to learn with its own difficulty ratings
            return_dict (bool): switch to return a dict with the difficulty detail in each category.
            detail_scores: return detail of scores as a string
        """
        # get dict of song difficulty in each category
        levels = song.get_level(self)
        # init variables
        diff = 0
        alphas = 0
        out_dict = {}
        # Compute the difficulty level for each category
        # based on the user's level
        for key in self.level.keys():
            a = self.level[key]   # user's level
            b = levels[key]       # song's level
        
            # get weighting coeff and difficulty function for each category
            alpha, func = self.challenges[key]
            # compute difficulty value for this category and this user
            diff_key = func(a, b)

            if diff_key is None:
                print(f'! {key} no rating')
                continue
            # use weighting coeff
            diff_key *= alpha
            # store coeff for further normalization
            alphas += alpha
            # increment final difficulty value
            # use absolute value because anything too easy
            # should also be discarded (though the term difficulty is no
            # longer meaningful in that case).
            diff += abs(diff_key)
            out_dict[key] = diff_key  # type: ignore
        # normalize
        diff /= alphas

        if detail_scores:
            detail = out_dict.items()
            return '[red]%7.2f[/red] %s' % (diff, guitor.song.str_levels(detail, format='%+4.2f', levels=list(self.level.keys())))

        if return_dict:
            return diff, out_dict  # type: ignore

        return diff

    def output(self,
               learnable: List[Song],
               nb: int = 5,
               oracle: dict = {},
               stats: dict = {}) -> dict:
        """output.
        Generate a readable dictionary of the user's profile
        and the song suggestions to display or use for 
        further processing.

        Args:
            self:
            learnable (List[Song]): learnable songs, sorted by challenge and difficulty level.
            nb (int): number of suggestions to return.
            oracle (dict): oracle dictionary to rate suggestions
            stats (dict): ??? not used?

        Returns:
            str:
        """
        out = deepcopy(self.profile)
        added = 0
        for song in learnable:
                json_dict = {
                        "id": song.id,
                        "tag": TAG_PROPOSAL,
                        "challenge": song.challenge,
                        "fit": round(song.fit, 3),
                        "scores": derich(self.song_difficulty(song, detail_scores=True)),
                        "Levels": derich(song.str_levels(self)),
                        }
                out['pieces'].append(json_dict)
                added += 1
                if added == nb:
                    break
                # Score against oracle
                if oracle:
                    if (self.name, str(song.id)) in oracle.keys():
                        oracle_cha = oracle[self.name, song.id]
                        if '?' in str(oracle_cha):
                            print(f'! Skip oracle: {self.name} {song.id} [yellow]{oracle_cha}')
                        else:
                            stats['score_nb'] += 1
                            if str(oracle_cha) == str(song.challenge):
                                stats['score'] += 1
                            else:
                                print(f"Bad prediction {song.id}: [red]{song.challenge}[/] ({oracle_cha})")
                    else:
                        oracle[self.name, str(song.id)] = f'???{song.challenge}'
        out['levels'] = self.level
        return out

    def csv_output(self, learnable, nb: int = 5) -> pd.DataFrame:
        """csv_output.
        Return a dataframe with suggested songs that
        can easily be written to a .csv file.

        Args:
            self:
            learnable: list of learnable songs, ordered by challenges and difficulty levels
            nb (int): number of suggestions to return

        Returns:
            pd.DataFrame: dataframe. Can be written to csv with df.to_csv()
        """
        df = pd.DataFrame(columns=['User', 'id', 'Song', 'Artist', 'Part',
                                   'Levels', 'Challenge', 'Test', 'Fit', 'Scores'])
        suggestions = self.output(learnable, nb)
        for p in suggestions['pieces']:
            row = [self.name, p['id'], "Unknown", "Unknown", "Unknown" ]
            if p['tag']=='proposal':
                row += [ p['Levels'], 'proposal-%d' % p['challenge'], '', p['fit'], p['scores'] ]
            else:
                row += [ '', p['tag'], '', '', '' ]
            df = pd.concat([df, pd.DataFrame([row], columns=df.columns)], ignore_index=True)
        return df
        
    def str_output(self, learnable: List[Song], 
                   nb: int=5,
                   by_challenge=True,
                   all_challenges=False,
                   detail_scores=False) -> str:
        """str_output.
        Return string formatted with rich to display directly in terminal.

        Args:
            self:
            learnable (List[Song]): list of learnable songs, ordered by challenge and difficulty.
            nb (int): number of suggestions to display. Default is 5.
            by_challenge: display songs ordered by challenges. Default is True.
            all_challenges: display all challenges. Default is False.
            detail_scores: enable detailed output of each scores. Default is False

        Returns:
            str: fully formatted string ready to display.
        """

        s = '## %s could learn the following songs\n' % self.name

        out_songs = []

        if by_challenge:
            for cha in User.source_challenges.keys():
                out_songs += [('### challenge %d ' % cha, 
                                list(filter(lambda song: (song.challenge == cha) or all_challenges,
                                            learnable)),
                                {'challenge': cha})]
        else:
            # All together
            out_songs += [('### selection ', learnable, None)]


        # Output
        format = '[yellow]%s[/]'
        format += '%s %s' if detail_scores else '[red]%5.2f[/red] %s'
        for category, songs, params in out_songs:
                s += category
                dif_songs = []
                for song in songs:
                    # Hack, reset the model challenge to the one used for this song
                    if params:
                        self.challenges = self.__class__.source_challenges[params['challenge']]
                    else:
                        self.challenges = self.__class__.source_challenges[song.challenge]
                    dif_songs.append((
                        song.challenge,
                        self.song_difficulty(song, detail_scores=detail_scores),
                        song.repr(self)
                    ))
                dif_songs.sort(key = lambda ss:ss[1])
                str_songs = [format % ss for ss in dif_songs]
                s += ellipsis_str(str_songs, start=nb, end=3, lines=True)
        return s

    def str_profile(self):
        """str_profile.
        Return a richly formatted string that summarizes one user's profile.
        Args:
            self:
        """
        self.compute_levels()
        s = '[bold]# %s[/bold]\n' % self.name
        s += "## Known songs %s\n" % ellipsis_str(sorted(self.songs_known, key=lambda s:-s.song_rating), lines=True)
        s += "## Current levels: %s\n" % guitor.song.str_levels(self.level.items(), long=True, levels=list(self.level.keys()))
        return s

    def str_stats(self):
        """str_stats.
        Returns the number of songs per tag in the user's profile.

        Args:
            self:
        """
        return str(tag_stats(self.profile))

    def __str__(self):
        return self.str_profile()
